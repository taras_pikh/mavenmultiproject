package customertimes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please write a path to a file.");
        writeFile(reader);
    }

    private static void writeFile(BufferedReader reader) {
        FileView fileView = new FileView();
        try {
            fileView.showFile(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    };
}
