package customertimes;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        File file = new File("./pom.xml");
        List<String> lines = FileUtils.readLines(file, "UTF-8");

        for (String line : lines) {
            System.out.println(line);
        }

    }
}
